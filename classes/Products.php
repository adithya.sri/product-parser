<?php 
namespace ProductParser\Classes;

class Products {
    public static $products = array();
    private static $limit = 1000;
    private static $con = null;
    
    static function Add($product){
        array_push(self::$products, $product);
        self::Save($product);
    }
    
    static function Count(){
        return count(self::$products);
    }

    static function Save($product){
        if (is_null(self::$con)){
            echo "Database connection is not assigned";
            return;
        }
        //self::InsertBulk($start_index);
        self::Insert($product);
    }

    static function DB($con){
        self::$con = $con;
    }

    static function InsertBulk($start_index){

        //Bulk Insert
        $question_marks = [];
        $data = array();
        $insert_values = array();
        $product_limit = min($start_index + self::getConstantLimit(), count(self::$products));
        for ($i = $start_index; $i < count(self::$products); $i++){
            $product = self::$products[$i];
            $data[] = array(
                'name' => $product->getName(), 
                'size' => $product->getSize(), 
                'type' => $product->getType(), 
                'color' => $product->getColor(),
                'description' => $product->getDescription(),
                'weight' => $product->getWeight(),
                'is_in_stock' => $product->getIsInStock(),
                'qty' => $product->getQty(),
                'price' => $product->getPrice(),
                'sku' => $product->getSKU()
            );
            array_push(
                $insert_values, 
                $product->getName(),
                $product->getSize(),
                $product->getType(),
                $product->getColor(),
                $product->getDescription(),
                $product->getWeight(),
                $product->getIsInStock(),
                $product->getQty(),
                $product->getPrice(),
                $product->getSKU()
            ); 
        }

        foreach($data as $d){
            $question_marks[] = '('  . self::placeholders('?', sizeof($d)) . ')';
        }


        self::$con->beginTransaction();
        $sql = "INSERT INTO products (name, size, type, color, description, weight, is_in_stock, qty, price, sku) VALUES " . implode(",", $question_marks);
        
        $stmt = self::$con->prepare ($sql);
        try {
            $stmt->execute($insert_values);
        } catch (PDOException $e){
            echo $e->getMessage();
        }
        self::$con->commit();
        
    }

    public static function Insert($product){
        //prepare variables for data insertion
        $name = $product->getName();
        $color = $product->getColor();
        $size = $product->getSize();
        $type = $product->getType();
        $color = $product->getColor();
        $description = $product->getDescription();
        $weight = $product->getWeight();
        $is_in_stock = $product->getIsInStock();
        $qty = $product->getQty();
        $price = $product->getPrice();
        $sku = $product->getSKU();

        //insert
        
        $sql = "INSERT INTO products (name, size, type, color, description, weight, is_in_stock, qty, price, sku) VALUES (:name, :size, :type, :color, :description, :weight, :is_in_stock, :qty, :price, :sku);";
        $stmt = self::$con->prepare ($sql);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':size', $size);
        $stmt->bindParam(':type', $type);
        $stmt->bindParam(':color', $color);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':weight', $weight);
        $stmt->bindParam(':is_in_stock', $is_in_stock);
        $stmt->bindParam(':qty', $qty);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':sku', $sku);

        try {
            $stmt->execute();
            $product->setId(self::$con->lastInsertId());
            //now add the images related to this id
            Products::SaveImages($product);
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    private static function SaveImages($product){
        $imagesCount = count($product->getImages());

         //Bulk Insert
         $question_marks = [];
         $data = array();
         $insert_values = array();
         for ($i = 0; $i < $imagesCount; $i++){
             //holds the data
             $data[] = array(
                 'product_id' => $product->getId(), 
                 'image_url' => $product->getImages()[$i], 
             );
             array_push(
                 $insert_values, 
                 $product->getId(),
                 $product->getImages()[$i]
            ); 
         }
         //prepare parameterized values
         foreach($data as $d){
             $question_marks[] = '('  . self::placeholders('?', sizeof($d)) . ')';
         }
 
         $sql = "INSERT INTO product_images (product_id, image_url) VALUES " . implode(",", $question_marks);
         $stmt = self::$con->prepare ($sql);

         try {
            self::$con->beginTransaction();
            $stmt->execute($insert_values);
            self::$con->commit();

         } catch (PDOException $e){
             echo $e->getMessage();
         }
    }

    private static function placeholders($text, $count=0, $separator=","){
        $result = array();
        if($count > 0){
            for($x=0; $x<$count; $x++){
                $result[] = $text;
            }
        }
    
        return implode($separator, $result);
    }

    public static function getConstantLimit(){
        return self::$limit;
    }
}
?>