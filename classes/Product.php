<?php
namespace ProductParser\Classes;

//Product class to handle data
class Product {
    private $name = "";
    private $description = "";
    private $type = "";
    private $size = "";
    private $color = "";
    private $sku = "";
    private $weight = 0;
    private $is_in_stock = 0;
    private $qty = 0;
    private $price = 0;
    private $images = array();
    private $id = 0; //assigned on insertion
    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        if (!isset($name))
            $name = '';
        
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        if (!isset($description))
            $description = '';
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of type
     */ 
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */ 
    public function setType($type)
    {
        if (!isset($type))
            $type = '';

        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of size
     */ 
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set the value of size
     *
     * @return  self
     */ 
    public function setSize($size)
    {
        if (!isset($size))
            $size = '';

        $this->size = $size;

        return $this;
    }

    /**
     * Get the value of color
     */ 
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set the value of color
     *
     * @return  self
     */ 
    public function setColor($color)
    {
        if (!isset($color))
            $color = '';

        $this->color = $color;

        return $this;
    }

    /**
     * Get the value of sku
     */ 
    public function getSKU()
    {
        return $this->sku;
    }

    /**
     * Set the value of sku
     *
     * @return  self
     */ 
    public function setSKU($sku)
    {
        if (!isset($sku))
            $sku = '';

        $this->sku = $sku;

        return $this;
    }

    /**
     * Get the value of weight
     */ 
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set the value of weight
     *
     * @return  self
     */ 
    public function setWeight($weight)
    {
        if (!isset($weight))
            $weight = '';

        $this->weight = doubleval($weight);

        return $this;
    }

    /**
     * Get the value of is_in_stock
     */ 
    public function getIsInStock()
    {
        return $this->is_in_stock;
    }

    /**
     * Set the value of is_in_stock
     *
     * @return  self
     */ 
    public function setIsInStock($is_in_stock)
    {
        if (!isset($is_in_stock))
            $is_in_stock = 0;

        $this->is_in_stock = $is_in_stock;

        return $this;
    }

    /**
     * Get the value of qty
     */ 
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set the value of qty
     *
     * @return  self
     */ 
    public function setQty($qty)
    {
        if (!isset($qty))
            $qty = 0;

        $this->qty = $qty;

        return $this;
    }

    /**
     * Get the value of price
     */ 
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     *
     * @return  self
     */ 
    public function setPrice($price)
    {
        if (!isset($price))
            $price = 0;
            
        $this->price = $price;

        return $this;
    }

    /**
     * Get the value of images
     */ 
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set the value of images
     *
     * @return  self
     */ 
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

     /**
     * Add an image
     *
     * @return  self
     */ 

     public function addImage($image)
     {
         array_push($this->images, $image);
     }
    

     public function __construct($product)
     {
         $this->setName($product->name);
         $this->setType($product->type);
         $this->setDescription($product->description);
         $this->setSize($product->size);
         $this->setColor($product->color);
         $this->setSKU($product->sku);
         $this->setWeight($product->weight);
         $this->setIsInStock((strtolower(trim($product->is_in_stock)) === "out of stock") ? 0 : 1);
         $this->setQty($product->qty);
         $this->setPrice($product->price);

        //all xml elements that start with image*

         $images = $product->xpath('*[starts-with(name(), "image")]');
         $imagesCount = count($images);
         
         for ($i = 0; $i < $imagesCount; $i++){
             $image = $images[$i];
             $this->addImage($image);
         }
         
     }


    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}