<?php
namespace ProductParser\Classes;
use \PDO;

class Connection
{

    private $server = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . "";
    private $user = DB_USERNAME;
    private $pass = DB_PASSWORD;
    private $options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, );
    private $con;

    public function __construct()
    {
        return $this->openConnection();
    }

    public function openConnection()
    {
        try {
            $this->con = new PDO($this->server, $this->user, $this->pass, $this->options);
            return $this->con;
        } catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }

    }

    public function closeConnection()
    {
        $this->con = null;
    }

    public function getConnection()
    {
        return $this->con;
    }

}

?>