# Product Parser

Loads products and it's corresponding images from an XML file into database.

### To Run

* `cd` into the directory and use `php -S localhost:8999`
* Change `config.php` to set your local MySQL credentials
* Create a database and then run the scripts to setup tables
* Visit `localhost:8999`
* 



```mysql
-- Create syntax for TABLE 'product_images'
CREATE TABLE `product_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `image_url` text,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=187281 DEFAULT CHARSET=utf8;

-- Create syntax for TABLE 'products'
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `type` varchar(255) DEFAULT '',
  `size` varchar(255) DEFAULT '',
  `color` varchar(255) DEFAULT '',
  `description` text,
  `weight` double(10,3) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `sku` varchar(255) DEFAULT '',
  `is_in_stock` tinyint(1) DEFAULT NULL,
  `date_added` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26064 DEFAULT CHARSET=utf8;
`