<?php
error_reporting(E_ALL);
set_time_limit(0); //script takes a bit more than 30 seconds on my personal computer, so I have set this. Should be avoided on production.

include_once("config.php");
include_once("classes/Connection.php");
include_once("classes/Products.php");
include_once("classes/Product.php");


use \ProductParser\Classes\Connection as Connection;
use \ProductParser\Classes\Products as Products;
use \ProductParser\Classes\Product as Product;
$db = new Connection();

$file_name = 'all_products.xml';
if (!file_exists($file_name)){
    echo "File does not exist";
    return;
}

$products = simplexml_load_string(file_get_contents($file_name), 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);
if ($products === false) {
    echo "Error loading the products file";
    return;
}

//count it once as we don't keep calling count() on for loop
$totalCount = count($products->channel->product);

Products::DB($db->getConnection());

for ($i = 0; $i < 1; $i++){
    $product_data = $products->channel->product[$i];
    $product = new Product($product_data);
    Products::Add($product);
}

echo "Completed adding images";

?>